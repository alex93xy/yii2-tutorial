<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.02.18
 * Time: 15:29
 */

namespace app\models;
use yii\db\ActiveRecord;


class Category extends ActiveRecord
{
    public static function tableName(){
        return 'categories';
    }

    public function getProducts(){
        return $this->hasMany(Product::className(), ['parent' => 'id']);
    }
}