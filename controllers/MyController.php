<?php
namespace app\controllers;


class MyController extends AppController{
  public function actionIndex($id = null){
    $hi = 'Hello Word!';
    $names = ['Ivaniv', 'Petrov', 'Sidorov'];
    return $this->render('index', ['hello' => $hi, 'names' => $names, 'id' => $id]);
  }
}
